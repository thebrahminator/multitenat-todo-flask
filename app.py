from flask import Flask, request
from query_trigger import trigger_creation
from typing import Dict
import json

app = Flask(__name__)
app.config['SERVER_NAME'] = "local.com:5000"


'''

For the multitenant to work, and enabling subdomain detection, one needs to add URL rule. 
Without server name, flask cannot detect subdomains in localhost. When putting this into production
Comment out this part. 

Go to /etc/hosts and add a rule. 
127.0.0.1 local.com

'''

@app.route('/')
def index():

    return "Hello World"

@app.route('/login/<merchant>/', methods=["POST"])
def detect_merchant(merchant):

    return f"The merchant is {merchant}"

@app.route('/create_db')
def create_databse():
    input_Dict: Dict = {}
    input_Dict['user_name'] = "Vishwanath"
    input_Dict['password'] = "beyblade"
    input_Dict['database'] = "random"
    value = trigger_creation(user_name=input_Dict['user_name'],
                             password=input_Dict['password'], database=input_Dict['database'])

    return f"Value successfully triggered {value}"

if __name__ == '__main__':
    app.run(debug=True)