import subprocess
from typing import List

def trigger_creation(*args, **kwargs):
    num_of_values = int(len(kwargs.keys()))
    arguments: List[str] = [None] *  (num_of_values + 1)
    arguments[0] = './scripts/create_db.sh'
    arguments[1] = kwargs['user_name']
    arguments[2] = kwargs['password']
    arguments[3] = kwargs['database']
    subprocess.run(args=arguments)

    return 1