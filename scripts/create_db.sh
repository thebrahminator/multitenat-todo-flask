#!/bin/bash

echo "First value is $1"
echo "Second value is $2"
echo "Third value is $3"

psql -c "CREATE USER $1 WITH PASSWORD '$2';"
psql -c "CREATE DATABASE $3;"
psql -c "GRANT ALL PRIVILEGES ON DATABASE $3 TO $1;"